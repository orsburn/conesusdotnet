*The actual readme is found in the **README** folder.  The more consistent nature of HTML documents over Markdown documents was favored for an elaborate readme file.*

# Origin

The idea for this project was originally hatched from an idea for creating scripts to automate the deployment of applications directly from Git repositories.  There is no real API support for Git, so a PHP wrapper around Git was born.

This project is a C# implementation of that wrapper library.

# Example

The **/src/Conesus.Example** project can be experimented with via the command line to see the types of output that's generated.

Run the example like:

```
dotnet run -p src/Conesus.Example
```

Tinker with **/src/Conesus.Example/Program.cs** to experiment with the various library calls.