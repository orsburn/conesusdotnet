﻿using System;
using ConesusDotNet;

namespace ConesusDotNet.Example
{
    class Program
    {
		static void Main(string[] args)
		{
			var con = new Conesus();

			if(args.Length > 0)
			{
				con.Repository = args[0];
			}
			else
			{
				con.Repository = "/path/to/repository";
			}

			// -----------------------------------------------------------------------------
			// LogEntry[] output = con.Log();

			// Console.WriteLine($"   Hash:  {output[0].Hash}");
			// Console.WriteLine($"Message:  {output[0].Message}");

			// -----------------------------------------------------------------------------
			foreach(string entry in con.RawLog())
			{
				Console.WriteLine(entry);
			}

			// -----------------------------------------------------------------------------
			// LogFile output = con.LogFiles()[2];

			// Console.WriteLine($"        Hash:  {output.Hash}");
			// Console.WriteLine($"      Status:  {output.Status}");
			// Console.WriteLine($"       Score:  {output.Score}");
			// Console.WriteLine($"        File:  {output.File}");
			// Console.WriteLine($"PreviousFile:  {output.PreviousFile}");

			// -----------------------------------------------------------------------------
			// Console.WriteLine(con.CommitForTag("1.6.22"));

			// -----------------------------------------------------------------------------
			// foreach(var entry in con.Commits())
			// {
			// 	Console.WriteLine(entry);
			// }
		}
    }
}