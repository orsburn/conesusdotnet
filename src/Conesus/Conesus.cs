﻿/* =============================================================================
This Stack Overflow answer explains how to use this library in another project.

	https://stackoverflow.com/a/50716032
============================================================================= */
using System;
using System.IO;


namespace ConesusDotNet
{
	public class Conesus
	{
		public string Repository;
		public int MinHashLength = 4;


		public Conesus(string repository = null)
		{
			Repository = repository;
		}


		public LogEntry[] Log()
		{
			string[] log = Run("git log --all --decorate --oneline").Split('\n');

			var results = new LogEntry[log.Length];
			int resultsIndex = 0;

			foreach(string entry in log)
			{
				int splitHere = entry.IndexOf(' ');

				var resultsInstance = new LogEntry();

				resultsInstance.Hash = entry.Substring(0, splitHere);
				resultsInstance.Message = entry.Substring(splitHere + 1);  // Plus 1 to skip the space we split on.

				results[resultsIndex ++] = resultsInstance;
			}

			return results;
		}


		public string[] RawLog()
		{
			return Run("git log --all --decorate --oneline").Split('\n');
		}


		public LogFile[] LogFiles()
		{
			string[] log = Run("git log --all --name-status --oneline --pretty=format:\"%H\"").Split('\n');
			var results = new LogFile[log.Length];
			string hash = null;
			int resultsIndex = 0;


			foreach(string entry in log)
			{
				var logFile = new LogFile();
				string[] fields = entry.Split('\t');

				if(fields.Length >= 2)
				{
					logFile.Hash = hash;
					logFile.Status = fields[0][0];

					if((fields[0].ToCharArray()[0] == 'R') || (fields[0].ToCharArray()[0] == 'C'))
					{
						logFile.Status = fields[0].ToCharArray()[0];
						logFile.Score = Int32.Parse(fields[0].Substring(1));
					}

					if(fields.Length == 3)
					{
						logFile.File = fields[2];
						logFile.PreviousFile = fields[1];  // If the file was renamed.
					}
					else
					{
						logFile.File = fields[1];
					}

					results[resultsIndex ++] = logFile;
				}
				else if(fields[0] != "")
				{
					hash = fields[0];
				}
			}

			return results;
		}


		public string[] Commits()
		{
			return Run("git log --all --decorate --oneline --pretty=format:\"%H\"").Split('\n');
		}


		public string CommitForTag(string tag)
		{
			/*
			This method works the way it does because there no good way to handle the case
			where the tag being sought doesn't exist.  Any such checks would likely require
			multiple calls to git on the system.  It's probably faster to just parse some
			git output.`

			Examples that have been tried:

				git tag -v [TAG]
				git rev-parse [TAG]
				git rev-list -n [TAG]
			*/
			string outputCommitID = null;
			string[] listing = Run("git show-ref --tags --dereference").Split('\n');

			foreach(string entry in listing)
			{
				string[] pieces = entry.Split(' ');
				string commitID = pieces[0];
				string tagRef = pieces[1];

				string tagCheck = tagRef.Substring(tagRef.LastIndexOf('/') + 1);

				/*
				The entire list is scanned rather than tring to be clever about accommodating
				annotated tags.  Based on the listing from git log, it looks like the hash that
				should be output is the one with the annotation if one exists.
				*/
				if((tagCheck == tag) || (tagCheck == $"{tag}^{{}}"))  // BUG  This will likely need to be updated to allow content betweeb the curly braces.
				{
					outputCommitID = commitID;
				}
			}

			return outputCommitID;
		}


		private string Run(string command)
		{
		    // TODO  There needs to be some verification of the repo path here.  Perhaps a try/catch.
		    Directory.SetCurrentDirectory(Repository);

			System.Diagnostics.ProcessStartInfo processStartInfo;

			switch(Environment.OSVersion.Platform.ToString())
			{
				// TODO  The arragements here will need to be re-worked once this can be tested against a Mac and Windows boxes.
				case "Unix":
					processStartInfo  = new System.Diagnostics.ProcessStartInfo(
						command.Substring(0, command.IndexOf(" ")),  // We need to break out the git part of the
						command.Substring(command.IndexOf(" ") + 1)  // command.
					);
					break;
				default:
					processStartInfo = new System.Diagnostics.ProcessStartInfo(
						"powershell.exe",
						command
					);
					break;
			}

		    var proc = new System.Diagnostics.Process();

		    processStartInfo.RedirectStandardOutput = true;
		    processStartInfo.UseShellExecute = false;
		    processStartInfo.CreateNoWindow = true;

		    proc.StartInfo = processStartInfo;

		    proc.Start();

		    return proc.StandardOutput.ReadToEnd().Trim();
		}
	}
}