namespace ConesusDotNet
{
	public class LogFile
	{
		public string Hash;
		public char Status;
		public int Score;
		public string File;
		public string PreviousFile;  // If the file was renamed.
	}
}